# Test ExpressJS Guide

## 1. Clone Project

```
git clone https://kantekcom@bitbucket.org/kantekcom/test-expressjs.git
```

## 2. Import database ( create database name ```kentest```)

## 3. In root of project, run command:

```
npm install
```

```
node app.js
```

## 4. access http://localhost:9669 and test